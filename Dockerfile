# https://spring.io/guides/gs/spring-boot-docker/
#FROM openjdk:11
FROM adoptopenjdk/openjdk11:latest
VOLUME /tmp

#ARG DEPENDENCY_CLASS=target/Tracking-1.0
#COPY ${DEPENDENCY_CLASS}/WEB-INF/lib        /app/lib
#COPY ${DEPENDENCY_CLASS}/WEB-INF/classes    /app

#CMD ["java","-Dspring.profiles.active=${SPRING_PROFILE}","-cp","app:app/lib/*","mx.com.tecno.tracking.TrackingApplication"]

ARG DEPENDENCY_CLASS=target
COPY ${DEPENDENCY_CLASS}/Tracking-1.0.war  /home
CMD ["java", "-XX:InitialHeapSize=6815736", "-XX:MaxHeapSize=52428800", "-jar", "/home/Tracking-1.0.war"]