package mx.com.tecno.tracking;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
	
	 public void addViewControllers(ViewControllerRegistry registry) {
	        registry.addViewController("/").setViewName("index");
	        registry.addViewController("/login").setViewName("login");
	        registry.addViewController("/home.html");
	        registry.addViewController("/index.html");
	        registry.addViewController("/usuarios").setViewName("usuarios");
	        registry.addViewController("/agregar-usuario").setViewName("agregar-usuario");
	        registry.addViewController("/catalogos").setViewName("catalogos");
	        
	      
	        
	    }

}
