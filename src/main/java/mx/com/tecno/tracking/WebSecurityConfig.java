package mx.com.tecno.tracking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import mx.com.tecno.tracking.service.impl.DetalleUsuarioServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DetalleUsuarioServiceImpl detalleUsuarioServiceImpl;

	@Autowired
	private AuthenticationSuccessHandler manejadorDeAutenticacionExitosa;
	
	@Autowired
	private AuthenticationFailureHandler manejadorDeAutenticacionFallida;

	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
		.authorizeRequests()
		.antMatchers("/", "/index").permitAll()
		.antMatchers("/usuarios").permitAll()
		.antMatchers("/img/**").permitAll()
		.and()
            .formLogin()
	            .loginPage("/index")
	            .loginProcessingUrl("/j_spring_security_check")
	            .defaultSuccessUrl("/home.html")
	            .failureHandler(manejadorDeAutenticacionFallida)
	            .successHandler(manejadorDeAutenticacionExitosa)
	            .usernameParameter("j_username")
	            .passwordParameter("j_password")
	        .permitAll()
        .and()
	        .logout()
	            .logoutUrl("/j_spring_security_logout")
	            .logoutSuccessUrl("/index.html?logSucc=true").permitAll()
	            .invalidateHttpSession(true)
	            .deleteCookies("JSESSIONID")
	            .permitAll()
        .and()
            .sessionManagement()
	            .invalidSessionUrl("/index.html?sessionInvalid=true")
	            .sessionFixation().migrateSession()
	            .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
	            .maximumSessions(1)                         
	            .maxSessionsPreventsLogin(true)          
	            .expiredUrl("/index.html?expired=true")             
	            .sessionRegistry(sessionRegistry());   

	}

	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}

	public void configure(final WebSecurity web) throws Exception {

		web.ignoring()
		.antMatchers("/resources/**")
		.antMatchers("/css/**")
		.antMatchers("/js/**")
		.antMatchers("/webjars/**");
		
	}
	
	@Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }
	
    @Bean
    public DaoAuthenticationProvider authProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(detalleUsuarioServiceImpl);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }
    
    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
    

    @Bean
    public SessionRegistry sessionRegistry() {
        SessionRegistry sessionRegistry = new SessionRegistryImpl();
        return sessionRegistry;
    }
    

}
