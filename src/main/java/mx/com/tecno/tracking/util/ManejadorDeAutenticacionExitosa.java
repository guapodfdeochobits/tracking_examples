package mx.com.tecno.tracking.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.service.UsuarioService;

@Component
@Slf4j
public class ManejadorDeAutenticacionExitosa implements AuthenticationSuccessHandler {


	private static final String ROLE_USUARIO = "ROLE_USUARIO";

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Value("${server.servlet.session.timeout}")
	private int TIMEOUT_SESSION;
	
	@Autowired
	private UsuarioService usuarioService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		handle(request, response, authentication);
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.setMaxInactiveInterval(TIMEOUT_SESSION);
		}
		clearAuthenticationAttributes(request);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Usuario usuario = usuarioService.obtenerUsuarioPorCorreo(auth.getName());
		
		usuario.setFechaHoraInicioSesion(Calendar.getInstance().getTime());
		
		usuarioService.guardarUsuarioRegistrado(usuario);

	}

	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
		String targetUrl = determineTargetUrl(authentication);

		if (response.isCommitted()) {
			if (log.isDebugEnabled())
				log.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	protected String determineTargetUrl(Authentication authentication) {
		boolean isUser = false;
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().equals(ROLE_USUARIO)) {
				isUser = true;
			} 
		}
		if (isUser) {
			return "/home.html";
		} else {
			throw new IllegalStateException();
		}
	}

	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

}
