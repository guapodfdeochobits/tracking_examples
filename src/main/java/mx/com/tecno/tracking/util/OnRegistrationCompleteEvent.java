package mx.com.tecno.tracking.util;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;
import mx.com.tecno.tracking.domain.Usuario;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1153101858486208854L;
	private final String appUrl;
    private final Locale locale;
    private final Usuario user;
 
    public OnRegistrationCompleteEvent(Usuario user, Locale locale, String appUrl) {
        super(user);
         
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
    }

	public String getAppUrl() {
		return appUrl;
	}

	public Locale getLocale() {
		return locale;
	}

	public Usuario getUser() {
		return user;
	}
    
    
}
