package mx.com.tecno.tracking.util;

import java.util.UUID;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.service.MailService;
import mx.com.tecno.tracking.service.UsuarioService;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private MessageSource messages;

	@Autowired
	private MailService mailService;

	@Autowired
	private Environment env;

	public void onApplicationEvent(OnRegistrationCompleteEvent event) {

		this.confirmRegistration(event);

	}

	private void confirmRegistration(OnRegistrationCompleteEvent event) throws MailException {
		Usuario user = event.getUser();
		String token = UUID.randomUUID().toString();
		usuarioService.crearTokenDeVerificacionParaUsuario(user, token);

		final SimpleMailMessage email = constructEmailMessage(event, user, token);
		
		try {
			mailService.send(email);
		} catch (MessagingException e) {
			throw new MailSendException("Error al enviar el correo: " + e.getMessage());
		}
	}

	private final SimpleMailMessage constructEmailMessage(final OnRegistrationCompleteEvent event, final Usuario user,
			final String token) {
		final String recipientAddress = user.getCorreoElectronico();
		final String subject = messages.getMessage("message.subject", null, event.getLocale());
		;
		final String confirmationUrl = event.getAppUrl() + "/usuario/confirmar?token=" + token;
		final String message = messages.getMessage("message.regSucc", null, event.getLocale());
		final String footerContact = messages.getMessage("message.footer.contact", null, event.getLocale());
		final String footerUrl = messages.getMessage("message.footer.url", null, event.getLocale());
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message + " \r\n" + confirmationUrl + " \r\n\n" + footerContact + "\r\n\n" + footerUrl);
		email.setFrom(env.getProperty("support.email"));
		return email;
	}

}
