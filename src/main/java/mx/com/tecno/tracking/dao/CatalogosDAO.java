package mx.com.tecno.tracking.dao;

import java.util.List;

public interface CatalogosDAO {

	public <T> List<T> obtenerCatalogo(Class<?> clase);
}
