package mx.com.tecno.tracking.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import mx.com.tecno.tracking.dao.CatalogosDAO;

@Repository
public class CatalogosDAOImpl implements CatalogosDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerCatalogo(Class<?> clase) {

		return entityManager.createQuery("from " + clase.getName() + " order by id").getResultList();
	}

}
