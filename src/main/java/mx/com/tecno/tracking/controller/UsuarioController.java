package mx.com.tecno.tracking.controller;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.domain.Compania;
import mx.com.tecno.tracking.domain.Role;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.VerificacionToken;
import mx.com.tecno.tracking.error.EmailExistsException;
import mx.com.tecno.tracking.service.CatalogoService;
import mx.com.tecno.tracking.service.UsuarioService;
import mx.com.tecno.tracking.util.OnRegistrationCompleteEvent;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/usuario")
@ApiIgnore
@Slf4j
public class UsuarioController {

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private MessageSource messages;
	
	@Autowired
	private CatalogoService catalogoService;
	

	private Usuario registrado;
	
	private ServletContext servletContext;
	
	
	public UsuarioController(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

	@PostMapping(value="/agregar")
	public String registrarNuevaCuentaUsuario(@ModelAttribute Usuario user, @RequestParam("rol") List<String> roles, HttpServletRequest request) {

		if (log.isDebugEnabled())
			log.debug("Registrando cuenta de usuario con informacion: {}", user);

		Locale local = request.getLocale();	

		try {
			registrado = crearCuentaUsuario(user, roles);
		} catch (EmailExistsException e1) {
			return "redirect:/agregar-usuario?error=" + messages.getMessage("message.email.repeat", null, local);
		}

		if (registrado == null && registrado.getId() == null) {
			return "redirect:/agregar-usuario?error=" + messages.getMessage("error.register.content", null, local);
		}

		final String appUrl = "http://" + request.getServerName() + request.getContextPath();
		
		try {
			eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registrado, request.getLocale(), appUrl));
		} catch (MailException e) {
			usuarioService.borrarUsuario(registrado);
			log.error(e.getMessage());
			return "redirect:/agregar-usuario?error=" + messages.getMessage("error.register.content", null, local);
		}

		return "redirect:/agregar-usuario?exitoso=true";
	}
	
    @GetMapping(value = "/confirmar")
    public String confirmarRegistro(WebRequest request, final Model model, @RequestParam("token") final String token) {
    	if(log.isDebugEnabled())
        	log.debug("confirmarRegistro token: {}", token);
    	Locale locale = request.getLocale();
        final VerificacionToken verificacionToken = usuarioService.obtenerVerificacionToken(token);
        if (verificacionToken == null) {
        	if(log.isDebugEnabled())
            	log.debug("token de verficacion nulo");
            final String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:/badUser.html?lang=" + locale.getLanguage()+"&message="+message;
        }
        
        final Usuario usuario = verificacionToken.getUsuario();
        final Calendar cal = Calendar.getInstance();
        if ((verificacionToken.getFechaCaducidad().getTime() - cal.getTime().getTime()) <= 0) {
        	final String message = messages.getMessage("auth.message.expired", null, locale);
            model.addAttribute("message", message);
            model.addAttribute("expired", true);
            model.addAttribute("token", token);
            return "redirect:/badUser.html?lang=" + locale.getLanguage()+"&message="+message+"&expired=true"+"&token="+token;
        }

        usuario.setHabilitado(true);
        usuarioService.guardarUsuarioRegistrado(usuario);
        final String message = messages.getMessage("message.accountVerified", null, locale);
        model.addAttribute("message", message);

        return "redirect:/index?lang=" + locale.getLanguage()+"&message="+message;
    }

    @GetMapping("/listar")
	public @ResponseBody List<Usuario> obtenerTodosUsuarios(ModelMap modelMap){
		List<Usuario> usuarios = usuarioService.obtenerTodosUsuarios();
		return usuarios;
	}

	@PutMapping("/habilitar/{id}")
	public @ResponseBody Usuario usuarioActivado(@RequestBody Usuario usuario,@PathVariable String id){
		Usuario usuarioActual = this.usuarioService.obtenerUsuarioPorId(id);
		// usuarioActual.setApellido(usuario.getApellido());
		usuarioActual.setHabilitado(usuario.isHabilitado());
		usuarioService.habilitarUsuario(usuarioActual);
		return usuarioActual;
	}


    @PutMapping(value="/actualizar")
    public String actualizarUsuario(@ModelAttribute Usuario user) {
    	
    	if(log.isDebugEnabled())
    		log.debug(">Entrando a actualizarUsuario<");
    	
    	Usuario usuarioCambiado = usuarioService.obtenerUsuarioPorCorreo(user.getCorreoElectronico());
    	
    	
    	usuarioService.guardarUsuarioRegistrado(usuarioCambiado);
    	
    	return "redirect:/index";
    	
    }
    
    
    @DeleteMapping(value="/borrar/{usuarioId}")
    public String borrarUsuario(@PathVariable String usuarioId) {
    	
    	if(log.isDebugEnabled())
    		log.debug(">Entrando a borrarUsuario<");
    	
    	Usuario usuario = usuarioService.obtenerUsuarioPorId(usuarioId);
    	
    	
    	usuarioService.borrarUsuario(usuario);
    	
    	return "redirect:/index";
    	
    }
    
    @GetMapping(value = "/cargar-agregar-usuario")
    public String cargarAgregarUsuario(HttpServletRequest request, Model model) {
    	
    	if(log.isDebugEnabled())
    		log.debug("Entrando a cargarAgregarUsuario");
    	
    	List<?> catalogo = catalogoService.obtenerCatalogo(Compania.class);
    	
    	
    	servletContext.setAttribute("companias", catalogo);
    	
    	catalogo = catalogoService.obtenerCatalogo(Role.class);
    	
    	
    	servletContext.setAttribute("roles", catalogo);

    	return "redirect:/agregar-usuario";
    }

	private Usuario crearCuentaUsuario(final Usuario usuarioDto, List<String> roles) throws EmailExistsException {
		Usuario registrado = null;
		
			usuarioDto.setRoles(usuarioService.obtenerRolesPorListaNombres(roles));
			registrado = usuarioService.registrarNuevaCuentaUsuario(usuarioDto);
		
		return registrado;
	}

}
