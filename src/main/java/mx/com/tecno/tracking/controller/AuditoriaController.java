package mx.com.tecno.tracking.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.service.AuditoriaService;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/auditoria")
@ApiIgnore
@Slf4j
public class AuditoriaController {
	
	@Autowired
	private AuditoriaService auditoriaService;
	
	@GetMapping(value = "/obtenerToda")
	public String obtenerAuditorias(HttpServletRequest request, Model model) {
		
		if(log.isDebugEnabled())
			log.debug(">Entrando a AuditoriaController.obtenerAuditorias()<");
		
		int page = 0; //default page number is 0 (yes it is weird)
        int size = 10; //default page size is 10
        
        
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }
        
        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        
        model.addAttribute("revisiones", 
        		auditoriaService.buscarRevisionesParaAuditoria(PageRequest.of(page, size)));
        
        return "index.html";
	  } 

}
