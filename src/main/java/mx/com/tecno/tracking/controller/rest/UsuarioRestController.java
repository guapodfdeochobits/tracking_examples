package mx.com.tecno.tracking.controller.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.jwt.JwtUtils;
import mx.com.tecno.tracking.service.impl.DetalleUsuarioServiceImpl;

@RestController
@RequestMapping("/api")
@Slf4j
public class UsuarioRestController {

	@Autowired
	private DetalleUsuarioServiceImpl detalleUsuarioServiceImpl;
	
	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping(value = "/usuario/autenticar")
	@ApiOperation(value = "Inicia")
	public Usuario autenticarUsuario(@RequestParam("correo") String correo, @RequestParam("contrasenia") String contrasenia) {

		if (log.isDebugEnabled())
			log.debug("Entrando a inciarSesion");

		User user = (User) detalleUsuarioServiceImpl.loadUserByUsername(correo);

		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getAuthorities());

		SecurityContextHolder.getContext().setAuthentication(auth);
		
		String jwt = jwtUtils.generateJwtToken(auth);
		
		/*List<String> roles = user.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());*/
		
		Usuario usuario = new Usuario();
		usuario.setCorreoElectronico(user.getUsername());
		//usuario.setRoles(roles);
		usuario.setTokenAutenticacion(jwt);

		return usuario;
	}

}
