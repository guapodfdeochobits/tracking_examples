package mx.com.tecno.tracking.controller;

import java.util.List;

import mx.com.tecno.tracking.domain.Compania;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.service.CatalogoService;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/catalogos")
@ApiIgnore
@Slf4j
public class CatalogosController {

    private static final String BASE_URL = "https://develop.alendumerp.mx/satest30092019/ws/com.alendum.custom.tracking2.transportistas";
    static RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private CatalogoService catalogoService;

    @GetMapping(value = "/obtenerCatalogoPorNombre", produces = "application/json")
    @ResponseBody
    public List<?> obtenerCatalogo(@RequestParam("nombreCatalogo") final String nombreCatalogo) {

        if (log.isDebugEnabled())
            log.debug(" CatalogosController.obtenerCatalogo");

        List<?> catalogo = catalogoService.obtenerCatalogo(nombreCatalogo.getClass());


        return catalogo;
    }

    @GetMapping("/companias")
    private @ResponseBody
    List<Compania> listarCompanias() {
        List<Compania> companias = catalogoService.obtenerTodasCompanias();
        System.out.println(companias);
        return companias;
    }

    @PostMapping("/companias_editar/{id}")
    private @ResponseBody
    Compania editarCompania(@RequestBody Compania compania, @PathVariable String id) {
        Compania companiaActual = this.catalogoService.obtenerCompaniaPorId(id);
        companiaActual.setRazonSocial(compania.getRazonSocial());
        catalogoService.editarCompania(companiaActual);
        return companiaActual;
    }
/*private @ResponseBody
    ResponseEntity<String> Clientes(){
    String plainCreds = "tracking2:tracking2020";
    byte[] plainCredsBytes = plainCreds.getBytes();
    byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
    String base64Creds = new String(base64CredsBytes);


    HttpHeaders headers = new HttpHeaders();
    headers.add("Authorization", "Basic " + base64Creds);
    HttpEntity<String> request = new HttpEntity(headers);
    return this.restTemplate.exchange(BASE_URL, HttpMethod.GET, request, String.class);
}*/
}
