package mx.com.tecno.tracking.error;

@SuppressWarnings("serial")
public class EmailExistsException extends Exception  {
	
	public EmailExistsException(String message) {
        super(message);
    }

}
