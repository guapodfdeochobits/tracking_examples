package mx.com.tecno.tracking.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CompaniaRepository extends CrudRepository<Compania, Long> {
    @Query("select c from Compania c where c.razonSocial like %?1")
Compania findByRazonSocialContaining(String razon_social);
}
