package mx.com.tecno.tracking.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import lombok.Data;

@Entity
@Audited
@Data
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(max=100)
	private String nombre;
	
	@NotNull
	@Size(max=100)
	private String apellido;
	
	private String nombreCompleto;
	
	@NotNull
	private String correoElectronico;
	
	@NotNull
	@Size(max=100)
	private String movil;
	
	
	@JoinTable(name = "usuarios_roles", 
			joinColumns = {
					@JoinColumn(name = "usuario_id", referencedColumnName = "id", 
							foreignKey = @ForeignKey(name ="USUARIO_FK")
					) }, 
					inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id", 
							foreignKey = @ForeignKey(name ="ROLE_FK")) },
					foreignKey = @ForeignKey(name ="USUARIO_FK"),
					inverseForeignKey = @ForeignKey(name ="ROLE_FK")
			)
	@ManyToMany(fetch=FetchType.EAGER)
	@NotAudited
	private List<Role> roles = new ArrayList<Role>();
	
	
	@NotNull
	@Size(max=100)
	private String tipoUsuario;

	@NotNull
	@NotAudited
	private String password;
	
	private Date fechaHoraCreacion;
	
	private Date fechaHoraEdicion;
	
	@NotAudited
	private Date fechaHoraInicioSesion;
	
	@OneToOne
	private Compania compania;

	@NotNull
	private boolean habilitado;
	
	@NotNull
	private boolean tokenExpired;
	
	@Transient
	private String tokenAutenticacion;


	public Usuario() {
		super();
		this.habilitado=false;
		this.tokenExpired = false;
	}
	
	@PrePersist()
	public void prePersist() {
		this.setNombreCompleto(this.getNombre() + " " + this.getApellido());
		this.setFechaHoraCreacion(Calendar.getInstance().getTime());
	}
	
	@PreUpdate
	public void preUpdate() {
		this.setFechaHoraEdicion(Calendar.getInstance().getTime());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
}
