package mx.com.tecno.tracking.domain;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class PasswordResetToken {
	
	private static final int EXPIRACION = 60 * 24;
	
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
    
    
    private String token;
    
    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "usuario_id")
    private Usuario usuario;
    
    private Date fechaCaducidad;
    
    public PasswordResetToken() {
        super();
    }

    public PasswordResetToken(String token) {
        super();

        this.token = token;
        this.fechaCaducidad = calcularFechaDeCaducidad(EXPIRACION);
    }

    public PasswordResetToken(String token, Usuario usuario) {
        super();

        this.token = token;
        this.usuario = usuario;
        this.fechaCaducidad = calcularFechaDeCaducidad(EXPIRACION);
    }
    
    
    private Date calcularFechaDeCaducidad(int tiempoCaducidadEnMinutos) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        cal.add(Calendar.MINUTE, tiempoCaducidadEnMinutos);
        return new Date(cal.getTime().getTime());
    }
	
}
