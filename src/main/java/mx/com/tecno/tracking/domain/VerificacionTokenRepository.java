package mx.com.tecno.tracking.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface VerificacionTokenRepository extends CrudRepository<VerificacionToken, Long> {
	
	VerificacionToken findByToken(String token);
	
	VerificacionToken findByUsuario(Usuario usuario);
	
	@Query("select vt from VerificacionToken vt where vt.usuario.correoElectronico=:correo")
	VerificacionToken findByEmail(@Param("correo") String correo);

}
