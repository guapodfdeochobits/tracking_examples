package mx.com.tecno.tracking.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface AuditoriaRevisionRepository extends CrudRepository<AuditoriaRevision, Long> {

	Page<AuditoriaRevision> findAll(Pageable pageable);
}
