package mx.com.tecno.tracking.domain;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
public class VerificacionToken {
	
	private static final int EXPIRACION = 60 * 24;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String token;
	
	@OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "usuario_id")
    private Usuario usuario;
	
	@NotNull
	private Date fechaCaducidad;
	
	@NotNull
	private boolean verificado;
	
	
	public VerificacionToken() {
        super();
    }
    public VerificacionToken(String token, Usuario usuario) {
        super();
        this.token = token;
        this.usuario = usuario;
        this.fechaCaducidad = calcularFechaDeCaducidad(EXPIRACION);
        this.verificado = false;
    }
     
    private Date calcularFechaDeCaducidad(int tiempoCaducidadEnMinutos) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, tiempoCaducidadEnMinutos);
        return new Date(cal.getTime().getTime());
    }

}
