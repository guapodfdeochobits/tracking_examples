package mx.com.tecno.tracking.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;

import org.hibernate.envers.DefaultTrackingModifiedEntitiesRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionType;

import lombok.Getter;
import lombok.Setter;

@Entity
@RevisionEntity(AuditoriaRevisionListener.class)
public class AuditoriaRevision extends DefaultTrackingModifiedEntitiesRevisionEntity {

	private static final long serialVersionUID = 1L;
	
	
	private String quienRealizoAccion;
	

	private Date fechaDeAccion;
	
	
	@ElementCollection(fetch=FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @Column(name="REVTYPE")
    @MapKeyColumn(name="NAMEID")
    @CollectionTable(name="ENT_REVTYPE", joinColumns=@JoinColumn(name="REV"))
    @Getter @Setter
    private Map<String,RevisionType> entityRevType = new HashMap<>();
	

	public static String llave(Class<?> entityClass, Serializable entityId) {
	        return String.format("%s-%s",entityClass.getSimpleName(),entityId.toString());
	}

	public String getQuienRealizoAccion() {
		return quienRealizoAccion;
	}


	public void setQuienRealizoAccion(String quienRealizoAccion) {
		this.quienRealizoAccion = quienRealizoAccion;
	}


	public Date getFechaDeAccion() {
		return fechaDeAccion;
	}


	public void setFechaDeAccion(Date fechaDeAccion) {
		this.fechaDeAccion = fechaDeAccion;
	}


	@Override
	public String toString() {
		return "Revision [quienRealizoAccion=" + quienRealizoAccion + ", fechaDeAccion=" + fechaDeAccion +  "id=" + this.getId() + "  modifiedEntityNames:" +  this.getModifiedEntityNames() + " entityRevType:" + this.getEntityRevType() + "]";
	}
	

}
