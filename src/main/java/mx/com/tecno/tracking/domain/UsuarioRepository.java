package mx.com.tecno.tracking.domain;

import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	Usuario findByCorreoElectronico(String email);
}
