package mx.com.tecno.tracking.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.envers.Audited;

import lombok.Data;

@Entity
@Audited
@Data
public class Compania {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	private String razonSocial;
	
	private String alias;
	
	private String localidad;
	
	private String rfc;
	
	private String direccion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Generated(value=GenerationTime.INSERT)
	private Date fechaHoraCreacionCia;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Generated(value=GenerationTime.ALWAYS)
	private Date fechaHoraEdicionCia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaHoraCreacionCia() {
		return fechaHoraCreacionCia;
	}

	public void setFechaHoraCreacionCia(Date fechaHoraCreacionCia) {
		this.fechaHoraCreacionCia = fechaHoraCreacionCia;
	}

	public Date getFechaHoraEdicionCia() {
		return fechaHoraEdicionCia;
	}

	public void setFechaHoraEdicionCia(Date fechaHoraEdicionCia) {
		this.fechaHoraEdicionCia = fechaHoraEdicionCia;
	}
}
