package mx.com.tecno.tracking.domain;

import org.springframework.data.repository.CrudRepository;

public interface PrivilegioRepository extends CrudRepository<Privilegio, Long> {

}
