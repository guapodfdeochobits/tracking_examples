package mx.com.tecno.tracking.domain;

import org.springframework.data.repository.CrudRepository;

public interface PasswordResetTokenRepository extends CrudRepository<PasswordResetToken, Long> {
	
	PasswordResetToken findByUsuario(Usuario usuario);

}
