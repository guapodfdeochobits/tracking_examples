package mx.com.tecno.tracking.domain;

import java.io.Serializable;
import java.util.Calendar;

import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionListener;
import org.hibernate.envers.RevisionType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
public class AuditoriaRevisionListener implements RevisionListener, EntityTrackingRevisionListener {
	

	@Override
	public void newRevision(Object revisionEntity) {
		
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User authenticatedUser = (User) authentication.getPrincipal(); 
		
		
		AuditoriaRevision revision = (AuditoriaRevision) revisionEntity;
		
		revision.setQuienRealizoAccion(authenticatedUser.getUsername());
		revision.setFechaDeAccion(Calendar.getInstance().getTime());
		

	}

	@Override
	public void entityChanged(Class entityClass, String entityName, Serializable entityId, RevisionType revisionType,
			Object revisionEntity) {
		if(log.isDebugEnabled())
			log.debug("Entrando a entityChanged");
		AuditoriaRevision revision = (AuditoriaRevision) revisionEntity;
		if(log.isDebugEnabled())
			log.debug("tipo:" + revisionType.toString());
		revision.getEntityRevType().put(AuditoriaRevision.llave(entityClass, entityId), revisionType);
	}

}
