package mx.com.tecno.tracking.service;

import mx.com.tecno.tracking.domain.Compania;

import java.util.List;

public interface CatalogoService {

		List<?> obtenerCatalogo(Class<?> clase);
		List<Compania>obtenerTodasCompanias();
		Compania editarCompania(Compania compania);
		Compania obtenerCompaniaPorId(String id);
}
