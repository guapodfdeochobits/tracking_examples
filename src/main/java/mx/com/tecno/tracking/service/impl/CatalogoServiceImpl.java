package mx.com.tecno.tracking.service.impl;

import java.util.List;

import mx.com.tecno.tracking.domain.Compania;
import mx.com.tecno.tracking.domain.CompaniaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.tecno.tracking.dao.CatalogosDAO;
import mx.com.tecno.tracking.service.CatalogoService;

@Service
public class CatalogoServiceImpl implements CatalogoService {
	
	@Autowired
	private CatalogosDAO catalogosDAO;
	@Autowired
	private CompaniaRepository companiaRepository;
 
	@Override
	public List<?> obtenerCatalogo(Class<?> clase) {
		
		return catalogosDAO.obtenerCatalogo(clase);
	}

	@Override
	public List<Compania> obtenerTodasCompanias() {
		return (List<Compania>) companiaRepository.findAll();
	}

	@Override
	public Compania editarCompania(Compania compania) {
		return companiaRepository.save(compania);
	}

	@Override
	public Compania obtenerCompaniaPorId(String id) {
		return companiaRepository.findById(Long.parseLong(id)).get();
	}

}
