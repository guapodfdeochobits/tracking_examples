package mx.com.tecno.tracking.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.com.tecno.tracking.domain.AuditoriaRevision;

public interface AuditoriaService {
	
	Page<AuditoriaRevision> buscarRevisionesParaAuditoria(Pageable pageable);

}
