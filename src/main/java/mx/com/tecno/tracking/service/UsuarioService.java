package mx.com.tecno.tracking.service;

import java.util.List;

import mx.com.tecno.tracking.domain.Role;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.VerificacionToken;
import mx.com.tecno.tracking.error.EmailExistsException;

public interface UsuarioService {

	Usuario registrarNuevaCuentaUsuario(Usuario usuario) throws EmailExistsException;
	
	void crearTokenDeVerificacionParaUsuario(Usuario usuario, String token);
		
	void borrarUsuario(Usuario usuario);
	
	VerificacionToken obtenerVerificacionToken(String verificacionToken);
	
	 void guardarUsuarioRegistrado(Usuario usuario);

	List<Usuario>obtenerTodosUsuarios();

	void habilitarUsuario(Usuario usuario);
	 
	 List<Role> obtenerRolesPorListaNombres(List<String> roles);
	 
	 Usuario obtenerUsuarioPorCorreo(String email);
	 
	 Usuario obtenerUsuarioPorId(String id);
	 
	 Usuario editarUsuario(Usuario usuario, List<String> roles);
	
}
