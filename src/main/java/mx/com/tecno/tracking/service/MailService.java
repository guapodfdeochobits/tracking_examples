package mx.com.tecno.tracking.service;

import javax.mail.MessagingException;

import org.springframework.mail.SimpleMailMessage;

public interface MailService {

	public void send(String to, String subject, String text) throws MessagingException;
	
	public void send(SimpleMailMessage message) throws MessagingException;
	
}
