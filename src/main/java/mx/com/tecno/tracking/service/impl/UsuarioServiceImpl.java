package mx.com.tecno.tracking.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.domain.PasswordResetToken;
import mx.com.tecno.tracking.domain.PasswordResetTokenRepository;
import mx.com.tecno.tracking.domain.Role;
import mx.com.tecno.tracking.domain.RoleRepository;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.UsuarioRepository;
import mx.com.tecno.tracking.domain.VerificacionToken;
import mx.com.tecno.tracking.domain.VerificacionTokenRepository;
import mx.com.tecno.tracking.error.EmailExistsException;
import mx.com.tecno.tracking.service.UsuarioService;

@Service
@Slf4j
public class UsuarioServiceImpl implements UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private VerificacionTokenRepository verificacionTokenRepository;

	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;


	public Usuario registrarNuevaCuentaUsuario(Usuario usuario) throws EmailExistsException {
		
		if(log.isDebugEnabled())
			log.debug(">Entrando a registrarNuevaCuentaUsuario<");

		
		if(existeEmail(usuario.getCorreoElectronico())) {
			throw new EmailExistsException("Existe una cuenta con el mismo correo: " + usuario.getCorreoElectronico());
		}
		
		
		
		usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
		
		return usuarioRepository.save(usuario);
		
	}

	@Override
	public void crearTokenDeVerificacionParaUsuario(Usuario usuario, String token) {
		final VerificacionToken verificacionToken = new VerificacionToken(token, usuario);
		verificacionTokenRepository.save(verificacionToken);
		
	}

	@Override
	public void borrarUsuario(Usuario usuario) {
		
	  final VerificacionToken verificacionToken = verificacionTokenRepository.findByUsuario(usuario);
	  
	  if (verificacionToken != null) {
		  verificacionTokenRepository.delete(verificacionToken);
      }
	  
	  final PasswordResetToken passwordToken = passwordResetTokenRepository.findByUsuario(usuario);
	  
	  if (passwordToken != null) {
		  passwordResetTokenRepository.delete(passwordToken);
      }
		 
	  usuarioRepository.delete(usuario);		
	}

	@Override
	public VerificacionToken obtenerVerificacionToken(String verificacionToken) {
		return verificacionTokenRepository.findByToken(verificacionToken);
	}

	@Override
	public void guardarUsuarioRegistrado(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	@Override
	public List<Usuario> obtenerTodosUsuarios() {
		return (List<Usuario>) usuarioRepository.findAll();
	}

	@Override
	public void habilitarUsuario(Usuario usuario) {
		usuarioRepository.save(usuario);
	}


	private boolean existeEmail(final String email) {
        final Usuario usuario = usuarioRepository.findByCorreoElectronico(email);
        if (usuario != null) {
            return true;
        }
        return false;
    }

	@Override
	public List<Role> obtenerRolesPorListaNombres(List<String> roles) {
		
		List<Role> lista = new ArrayList<Role>();
		
		for (String role : roles) {
			lista.add(roleRepository.findByNombre(role));
		}
		
		return lista;
	}

	@Override
	public Usuario obtenerUsuarioPorCorreo(String email) {
		
		return usuarioRepository.findByCorreoElectronico(email);
	}

	@Override
	public Usuario obtenerUsuarioPorId(String id) {
		
		return usuarioRepository.findById(Long.parseLong(id)).get();
	}
	
	public Usuario editarUsuario(Usuario usuario, List<String> roles) {
		Optional<Usuario> userOp = usuarioRepository.findById(usuario.getId());
		List<Role> rolesObtenidos = obtenerRolesPorListaNombres(roles);
		
		Usuario userAux = userOp.get();
		userAux.setCorreoElectronico(usuario.getCorreoElectronico());
		userAux.setMovil(usuario.getMovil());
		userAux.setPassword(passwordEncoder.encode(usuario.getPassword()));
		userAux.setRoles(null);
		
		userAux.setRoles(rolesObtenidos);
		usuarioRepository.save(userAux);
		return userAux;
		
	}


	

}
