package mx.com.tecno.tracking.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.service.MailService;

@Service
@Slf4j
public class MailServiceImpl implements MailService {
	
	@Autowired
	private JavaMailSender mailSender;
	
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	

	/** envio de email 
	 * @param to correo electronico del destinatario
	 * @param subject asunto del mensaje
	 * @param text cuerpo del mensaje
	 * @throws MessagingException 
	 */
	public void send(String to, String subject, String text) throws MailException {
		if(log.isDebugEnabled())
			log.debug("Entrando a MailServiceImpl.send(String to, String subject, String text)");
		try {
		MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setTo(to);
        helper.setText(text);
        helper.setSubject(subject);
        mailSender.send(message);
		} catch (MessagingException e) {
			throw new MailSendException("Error al enviar el correo: " + e.getMessage());
		}
        
	}

	public void send(SimpleMailMessage message) throws MailException {
		mailSender.send(message);
		
	}

}
