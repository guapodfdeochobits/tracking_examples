package mx.com.tecno.tracking.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.domain.Privilegio;
import mx.com.tecno.tracking.domain.Role;
import mx.com.tecno.tracking.domain.RoleRepository;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.UsuarioRepository;

@Service
@Slf4j
public class DetalleUsuarioServiceImpl implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private IntentoInicioSesionServiceImpl intentoInicioSesionServiceImpl;
	
	@Autowired
	private HttpServletRequest request;
	

	@Override
	public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
		if(log.isDebugEnabled())
			log.debug("Entrando a loadUserByUsername");
		String ip = request.getRemoteAddr();
		
		if (intentoInicioSesionServiceImpl.isBlocked(ip)) {
            throw new RuntimeException("blocked");
        }
		try {
            final Usuario user = usuarioRepository.findByCorreoElectronico(email);
           
            if (user == null) {
            	 
                return new org.springframework.security.core.userdetails.User(" ", " ", true, true, true, true, getAuthorities(Arrays.asList(roleRepository.findByNombre("ROLE_USUARIO"))));
            }
            
            if (log.isDebugEnabled())
                log.debug("******** usuario*******: " + user.toString());

            return new org.springframework.security.core.userdetails.User(user.getCorreoElectronico(), user.getPassword(), user.isHabilitado(), true, true, true, getAuthorities(user.getRoles()));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
	}
	
	public final Collection<? extends GrantedAuthority> getAuthorities(final Collection<Role> roles) {
        return getGrantedAuthorities(getRoles(roles));
    }
	
    private final List<String> getPrivileges(final Collection<Role> roles) {
        final List<String> privileges = new ArrayList<String>();
        final List<Privilegio> collection = new ArrayList<Privilegio>();
        for (Role role : roles) {
            collection.addAll(role.getPrivilegios());
        }
        for (final Privilegio item : collection) {
            privileges.add(item.getNombre());
        }
        return privileges;
    }

    private final List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (final String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
    
    private final List<String> getRoles(final Collection<Role> roles) {
    	final List<String> finalRoles = new ArrayList<String>();
    	
    	for (Role role : roles) {
            finalRoles.add(role.getNombre());
        }
    	
    	return finalRoles;
    }
	
	

}
