package mx.com.tecno.tracking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.domain.AuditoriaRevision;
import mx.com.tecno.tracking.domain.AuditoriaRevisionRepository;
import mx.com.tecno.tracking.service.AuditoriaService;

@Service
@Slf4j
public class AuditoriaServiceImpl implements AuditoriaService {
	
	@Autowired
	private AuditoriaRevisionRepository auditoriaRevisionRepository;

	@Override
	public Page<AuditoriaRevision> buscarRevisionesParaAuditoria(Pageable pageable) {
		if(log.isDebugEnabled())
			log.debug("Entrando a AuditoriaServiceImpl.buscarRevisionesParaAuditoria()");
		
		return auditoriaRevisionRepository.findAll(pageable);
	}


	

}
