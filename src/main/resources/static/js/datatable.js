
function format ( d ) {
   let html = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\" style=\"padding-left:50px;\">";

    html+="<tr>"
    html+= "<td> <strong>Nombre:</strong>";
    html+= "<td>"+d.nombre+"</td>";
    html+="</tr>"
    html+="<tr>"
    html+= "<td> <strong>Apellido:</strong>";
    html+= "<td>"+d.apellido+"</td>";
    html+="</tr>"
    html+="<tr>"
    html+= "<td> <strong>Movil:</strong>";
    html+= "<td>"+d.movil+"</td>";
    html+="</tr>"

    html+="<tr>";
    html+= "<td> <strong>Roles:</strong></td>";


    for (var i = 0; i < d.roles.length; i++) {

        html+="<td>"+d.roles[i].descripcion+"</td>";
    }
    html+="</tr>";
    html+="<tr>";
    html+= "<td> <strong> Fecha de Creación:</strong></td>";
    html+= "<td>"+ moment(d.fechaHoraCreacion).format('HH:mm MMM D, YYYY')+"</td>"
    html+="</tr>";
    html+="<tr>";
    html+= "<td> <strong>Fecha de Edición:</strong></td>";
    html+= "<td>"+moment(d.fechaHoraEdicion).format('HH:mm MMM D, YYYY')+"</td>"
    html+="</tr>";
    html+= "<td> <strong>Inicio de Sesión:</strong></td>";
    html+= "<td>"+moment(d.fechaHoraInicioSesion).format('HH:mm MMM D, YYYY')+"</td>"
    html+="</tr>";

    html+="</table>";


    return html;

}

var editor;
var habilitado;

$(document).ready(function () {





    var tabla = $('#tabusuarios').DataTable({
        pageLength: 2,

   "createdRow": function (row, data, index) {

            //
    //.html('<input name="boton" type="checkbox" class="btn-info"/>');

       if(data.habilitado== true){
         $('td', row)
             .eq(6).html('<td class="text-center"> <button class="btn btn-sm mr-1 details-control"  \n' +
             '                placeholder="Info" title="Más Información del Usuario">\n' +
             '                <i class="fas fa-info-circle" style="font-size:20px;color:#28A745"></i>\n' +
             '              </button><button class="btn btn-sm mr-1 custom-btn"  \n' +
             '                placeholder="Editar usuario" title="Activar / Desactivar">\n' +
             '                <i class="fas fa-power-off" style="font-size:20px;color:#28A745"></i>\n' +
             '              </button></td>')
             .removeClass('custom-checkbox')


       }else {$('td', row)
           .eq(6).html('<td class="text-center"> <button class="btn btn-sm mr-1 details-control"  \n' +
               '                placeholder="Info" title="Más Información del Usuario">\n' +
               '                <i class="fas fa-info-circle" style="font-size:20px"></i>\n' +
               '              </button><button class="btn btn-sm mr-1 custom-checkbox"  \n' +
               '                placeholder="Editar usuario" title="Activar / Desactivar">\n' +
               '                <i class="fas fa-power-off" style="font-size:20px"></i>\n' +
               '              </button></td>').removeClass('custom-btn')
       }
       },

        ordering: false,

        language: {
            zeroRecords: "<div class=\"alert alert-warning\"  id=\"warning\">!NO hay registros</div>",
            info: 'Se muestran de _START_ a _END_ de _TOTAL_ entradas',
            infoEmpty: 'Se muestran  _TOTAL_ entradas',
            infoFiltered: '(filtradas de un total de _MAX_ entradas)'
        },
    oLanguage:{
        oPaginate:{
            sPrevious:' Anterior',
            sNext: 'Siguiente'
        }

    },
        sDom: 'Rrtip',
        ajax:{

            url:'/usuario/listar',
            method: 'GET',

            dataSrc: ''
        },
        dataType: 'json',

        columns:[

            {data: 'id',searchable:false },
            {data: 'nombreCompleto',searchable:false},
            {data: 'correoElectronico'},
            {data: 'tipoUsuario', searchable:false},
            {data: 'compania.razonSocial',searchable:false},
            {data: 'habilitado',searchable:false},
            {data: null}

        ]


    })
    console.log( tabla.data())

    $('#tabusuarios tbody').on('click', '.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tabla.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
        console.log('row1',row.data())
    } );

    $('#tabusuarios_wrapper').hide();


    $('#button-addon2').on('keyup click', function() {
        $('#tabusuarios_wrapper').show();
     tabla.search($('#mySearchText').val(),true,false).draw();


    })

    tabla.on('keyup click', '.custom-btn', function () {
        var inactivo = false;
        var tr = $(this).closest('tr');
        var id = parseInt(tr.find('td:eq(0)').text());


        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: '/usuario/habilitar/'+ id,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({habilitado: inactivo}),

            success: function (data) {
                console.log('----->',data)

            }

        }).done(function (result) {
            tabla.ajax.reload(null,false);

        })

    });

    tabla.on('keyup click','.custom-checkbox',function () {
        var activo = true;
        var fila = $(this).closest('tr');
        var id = parseInt(fila.find('td:eq(0)').text());
        console.log(id);
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: '/usuario/habilitar/'+ id,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({habilitado: activo}),

            success: function (data) {
                console.log('----->',data)

            }

        }).done(function (result) {
            tabla.ajax.reload(null,false);

        })

    })

    var comp_id, opcion;
    opcion = 4;

var tabcatalogos = $('#tabcatalogos').DataTable({
    ordering: false,
    columnDefs:[{targets:[6,7], render:function(data){
            return moment(data).format('HH:mm MMM D, YYYY');
        }}],
    language: {
        zeroRecords: "<div class=\"alert alert-warning\"  id=\"warning\">No existen registros con los parametros de busqueda</div>",
        info: 'Se muestran de _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Se muestran  _TOTAL_ entradas',
        infoFiltered: '(filtradas de un total de _MAX_ entradas)'
    },
    oLanguage:{
        oPaginate:{
            sPrevious:'Anterior',
            sNext: 'Siguiente'
        }
    },
    sDom: 'Rrtip',
    ajax:{

        url:'/catalogos/companias',
        method: 'GET',
        data: {opcion:opcion},
        dataSrc: ''
    },
    dataType: 'json',

    columns:[

        {data: 'id',searchable:false },
        {data: 'alias',searchable:false},
        {data: 'direccion',searchable:false},
        {data: 'localidad',searchable:false},
        {data: 'razonSocial'},
        {data: 'rfc',searchable:false},
        {data: 'fechaHoraCreacionCia', searchable:false},
        {data: 'fechaHoraEdicionCia',searchable:false},
        {defaultContent: "<div class='text-center'><div class='btn-group'>" +
                "<button class='btn btn-primary btn-sm btnEditar'><i class='fas fa-edit'></i> </button> </div> </div>"}

    ]

})
    $('#formCompania').submit(function (e) {
e.preventDefault();
razon_s = $.trim($('#recipient-name').val())
console.log(razon_s)
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: '/catalogos/companias_editar/'+ comp_id,
            type: 'POST',
            datatype: 'json',
            data: JSON.stringify({razonSocial: razon_s}),
            success:function (data) {

            }
        }).done(function () {
            tabcatalogos.ajax.reload(null,false);
        })
        $('#exampleModal').modal('hide')
    })



    $(document).on('click', '.btnEditar', function () {
        opcion = 2;//editar
        fila = $(this).closest("tr");
        comp_id = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
        console.log('ID----',comp_id);
        razon_s= fila.find('td:eq(4)').text()
        console.log('Raz----',razon_s)
        $('#recipient-name').val(razon_s);
$('#exampleModal').modal('show')
    })
    $('#tabcatalogos_wrapper').hide();

    $('#btn-catalogos').on('keyup click', function() {
        $('#tabcatalogos_wrapper').show();
        tabcatalogos.search($('#mySearchText2').val()).draw();


    })
    console.log( tabcatalogos.data())
});
