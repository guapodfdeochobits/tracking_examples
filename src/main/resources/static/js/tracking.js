$(document).ready(function() {

	$("div.failure").fadeIn(300).delay(4000).fadeOut(400);

	$('#ingresar').click(function() {
		$('#overlay').fadeIn();
	});

	$.validator.addMethod("valueNotEquals", function(value, element, arg) {
		return arg !== value;
	}, "");
	
	jQuery.validator.addMethod("sololetrasyespacios", function(value, element) {
	    return this.optional(element) || /^[a-z\s]+$/i.test(value);
	}, "Solo letras y espacios");
	
	jQuery.validator.addMethod("sololetras", function(value, element) {
	    return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Solo letras");
	
	$.validator.addMethod("almenosundigitoletra", function(value, element) {
            return this.optional(element) || /[a-z].*[0-9]|[0-9].*[a-z]/i.test(value);
        }, 'Debe contener al menos una letra y un numero sin espacios');
	
	jQuery.validator.addMethod("sinespacios", function(value, element) { 
	    return value.indexOf(" ") < 0 && value != ""; 
	  }, "Sin espacios");
	
	jQuery.validator.setDefaults({
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	        error.addClass('invalid-feedback');
	        element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	        $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	        $(element).removeClass('is-invalid');
	    }
	});
	
	jQuery.extend(jQuery.validator.messages, {
		  required: "Este campo es obligatorio.",
		  remote: "Por favor, rellena este campo.",
		  email: "Por favor, escribe una dirección de correo válida",
		  url: "Por favor, escribe una URL válida.",
		  date: "Por favor, escribe una fecha válida.",
		  dateISO: "Por favor, escribe una fecha (ISO) válida.",
		  number: "Por favor, escribe un número entero válido.",
		  digits: "Por favor, escribe sólo dígitos.",
		  creditcard: "Por favor, escribe un número de tarjeta válido.",
		  equalTo: "Por favor, escribe el mismo valor de nuevo.",
		  accept: "Por favor, escribe un valor con una extensión aceptada.",
		  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
		  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
		  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
		  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
		  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
		  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
		});

	$("#agregar-usuario-forma").validate({
		
		rules: {
			
				nombre: {
					required:true,
					sololetrasyespacios:true,
					maxlength:100
				},
				apellido: {
					required:true,
					sololetras:true,
					maxlength:100
				},
				movil: {
					required:true,
					digits:true,
					maxlength:100
				},
			    'rol': {
			    	required: true,
			    	minlength: 1,
			    	maxlength: 2
			    },
				password: {
					sinespacios:true,
					required:true,
					almenosundigitoletra:true,
					minlength:8
					
				},
				inputContrasenia: {
					sinespacios:true,
					required:true,
					almenosundigitoletra:true,
					minlength:8,
					equalTo: '#password'
					
				}
				
		},
			  
        errorPlacement: function(error, element) {
        	if (element.attr('name') == 'rol') {
        	      error.insertAfter('#checkboxGroup');
        	 } else {
                error.appendTo(element.parent());
        	 }
              
         },
         submitHandler: function(form) {
				$('#overlay').fadeIn();
			    form.submit();
			  }
	});
	
    // ------------ Check passwords similarity --------------
    $('#password, #inputContrasenia').on('keyup', function () {
      if ($('#password').val() != '' && $('#inputContrasenia').val() != '' && $('#password').val() == $('#inputContrasenia').val() ) {
        $('#cPwdValid').show();
        $('#cPwdInvalid').hide();
        $('#cPwdInvalid').html('Los campos coinciden.').css('color', 'green');
        $('.myCpwdClass').addClass('is-valid');
        $('.myCpwdClass').removeClass('is-invalid');

      } else {
        $('#cPwdValid').hide();
        $('#cPwdInvalid').show();
        $('#cPwdInvalid').html('Los campos no coinciden.').css('color', 'red');
        $('.myCpwdClass').removeClass('is-valid');
        $('.myCpwdClass').addClass('is-invalid');
      }
    });

});
