package mx.com.tecno.tracking.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.service.impl.DetalleUsuarioServiceImpl;

@SpringBootTest(classes= {TrackingApplication.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class UsuarioTest {
	

	@Autowired
	private UsuarioRepository usuarioRepository;

	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private CompaniaRepository companiaRepository;
	
	
	@Autowired
	private DetalleUsuarioServiceImpl detalleUsuarioServiceImpl;
	
	
	@BeforeEach
	public void beforeEach() {
		
		User user = (User) detalleUsuarioServiceImpl.loadUserByUsername("oscar.ortiz@summitagromexico.com.mx");
		
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getAuthorities());
		
		SecurityContextHolder.getContext().setAuthentication(auth);
		
	}
	
	
	@Test
	public void debeGuardarUsuarioTest() {

		
		log.debug("Entrando a debeGuardarUsuarioTest");
		Role role = roleRepository.findByNombre("ROLE_USUARIO");
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		
		Usuario usuario = new Usuario();
		usuario.setNombre("Prueba");
		usuario.setApellido("Prueba");
		usuario.setCorreoElectronico("noreply.summit.tracking@gmail.com");
		usuario.setMovil("51-66-8-985");
		usuario.setRoles(roles);
		usuario.setTipoUsuario("Interno");
		usuario.setPassword("+Y(W7yaY");
		usuario.setFechaHoraInicioSesion(Calendar.getInstance().getTime());
		
		Compania compania = new Compania();
		compania.setRazonSocial("Semillas S.A de C.V");
		
		companiaRepository.save(compania);
		
		usuario.setCompania(compania);
		
		usuario.setHabilitado(true);
		usuario.setTokenExpired(false);	
		
		Usuario usuarioRecuperado = usuarioRepository.save(usuario);
		
		assertNotNull(usuarioRecuperado.getId());
		
		
	}
	
	@Test 
	public void debeActualizarUsuarioTest() {
		log.debug("Entrando al metodo UsuarioTest.debeActualizarUsuarioTest");
		Usuario usuario1 = new Usuario();
		usuario1.setCorreoElectronico("ingrid.casales@tecno.mx");
		usuario1.setMovil("556311239");
		usuario1.setNombre("Ingrid");
		usuario1.setApellido("Casales");
		usuario1.setTipoUsuario("Interno");
		usuario1.setPassword(passwordEncoder.encode("abc123"));
		Role role = roleRepository.findByNombre("ROLE_USUARIO");
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		usuario1.setRoles(roles);
		usuarioRepository.save(usuario1);
		usuario1.setMovil("5545677895");
		role = new Role();
		role = roleRepository.findByNombre("ROLE_ADMIN");
		roles.add(role);
		usuario1.setRoles(roles);
		usuarioRepository.save(usuario1);
		assertEquals("5545677895", usuario1.getMovil());
		assertTrue(usuario1.getRoles().size() > 1);
		
	}



	@Test
	public void debeHabilitarUsuario(){
		Usuario usuario2 = new Usuario();
		usuario2.setCorreoElectronico("victorprueba@tecno.com");
		usuario2.setMovil("556311239");
		usuario2.setNombre("Ingrid");
		usuario2.setApellido("Casales");
		usuario2.setTipoUsuario("Interno");
		usuario2.setHabilitado(true);
		usuario2.setPassword(passwordEncoder.encode("abc123"));
		Role role = roleRepository.findByNombre("ROLE_USUARIO");
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		usuario2.setRoles(roles);
		usuarioRepository.save(usuario2);
		usuario2.setMovil("5545677895");
		role = new Role();
		role = roleRepository.findByNombre("ROLE_ADMIN");
		roles.add(role);
		usuario2.setRoles(roles);
		usuarioRepository.save(usuario2);
		assertTrue(usuario2.isHabilitado() == true);

	}
	
	
	@AfterEach
	public void afterEach() {
		
		Usuario usuarioRecuperado = usuarioRepository.findByCorreoElectronico("noreply.summit.tracking@gmail.com");
		if(usuarioRecuperado != null)
			usuarioRepository.delete(usuarioRecuperado);
		
		usuarioRecuperado = usuarioRepository.findByCorreoElectronico("ingrid.casales@tecno.mx");
		if(usuarioRecuperado != null)
			usuarioRepository.delete(usuarioRecuperado);
		usuarioRecuperado = usuarioRepository.findByCorreoElectronico("victorprueba@tecno.com");
		if(usuarioRecuperado != null)
			usuarioRepository.delete(usuarioRecuperado);

	}


	
	
	

}
