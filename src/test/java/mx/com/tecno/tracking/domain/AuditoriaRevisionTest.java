package mx.com.tecno.tracking.domain;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.TrackingApplication;

@SpringBootTest(classes= {TrackingApplication.class})
@Slf4j
public class AuditoriaRevisionTest {
	
	@Autowired
	private AuditoriaRevisionRepository revisionRepository;
	
	@Test
	public void debeObtenerRevisionesTodas() {
		log.debug(">Entrando a debeObtenerRevisionesTodas()<");
		
		List<AuditoriaRevision> revisiones = (List<AuditoriaRevision>) revisionRepository.findAll();
		
		for (AuditoriaRevision revision : revisiones) {
			log.debug(revision.toString());
		}
		
	}

}
