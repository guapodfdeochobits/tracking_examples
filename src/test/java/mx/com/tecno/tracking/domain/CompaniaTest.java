package mx.com.tecno.tracking.domain;

import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.service.impl.DetalleUsuarioServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Calendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {TrackingApplication.class})
public class CompaniaTest {
    @Autowired
    private CompaniaRepository companiaRepository;
    @Autowired
    private DetalleUsuarioServiceImpl detalleUsuarioServiceImpl;

    @BeforeEach
    public void beforeEach() {

        User user = (User) detalleUsuarioServiceImpl.loadUserByUsername("oscar.ortiz@summitagromexico.com.mx");

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(auth);

    }

    @Test
    public void camposNoNulos() {
        Compania compania = new Compania();
        compania.setAlias("Ganaderos");
        compania.setRazonSocial("Ganaderos S.A");
        compania.setDireccion("Calle A");
        compania.setLocalidad("San F");
        compania.setRfc("43-asd-feeff");
        compania.setLocalidad("gdl");
        compania.setFechaHoraCreacionCia(Calendar.getInstance().getTime());
        Compania companiaCreada = companiaRepository.save(compania);
        assertThat(companiaCreada.getAlias()).isNotNull();
        assertThat(companiaRepository.findAll()).isNotNull();
        assertThat(companiaRepository.findByRazonSocialContaining("Ganaderos S.A"));
    }

    @AfterEach
    public void afterEach() {
       Compania companiaCreada = companiaRepository.findByRazonSocialContaining("Ganaderos S.A");
        if (companiaCreada != null) {
            companiaRepository.delete(companiaCreada);
        }
    }
}
