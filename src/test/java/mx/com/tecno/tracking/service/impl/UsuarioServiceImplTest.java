package mx.com.tecno.tracking.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.domain.Compania;
import mx.com.tecno.tracking.domain.CompaniaRepository;
import mx.com.tecno.tracking.domain.Role;
import mx.com.tecno.tracking.domain.RoleRepository;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.UsuarioRepository;
import mx.com.tecno.tracking.error.EmailExistsException;
import mx.com.tecno.tracking.service.UsuarioService;


@SpringBootTest(classes= {TrackingApplication.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class UsuarioServiceImplTest {

	@Autowired
	private UsuarioService  usuarioService;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	
	@Autowired
	private DetalleUsuarioServiceImpl detalleUsuarioServiceImpl;
	
	
	@Autowired
	private CompaniaRepository companiaRepository;
	

	private Role role;
	private Usuario usuario;
	
	
	@BeforeEach
	public void beforeEach() {
		
		User user = (User) detalleUsuarioServiceImpl.loadUserByUsername("oscar.ortiz@summitagromexico.com.mx");
		
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getAuthorities());
		
		SecurityContextHolder.getContext().setAuthentication(auth);
		
	}
	
	
	@Test
	@Order(1)
	public void debeRegistrarUsuarioTest() throws EmailExistsException {
		log.debug("Entrando a debeRegistrarUsuarioTest");
		
		
		User user = (User) detalleUsuarioServiceImpl.loadUserByUsername("oscar.ortiz@summitagromexico.com.mx");
		
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getAuthorities());
		
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		role = roleRepository.findByNombre("ROLE_USUARIO");
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		
		
		
		usuario = new Usuario();
		usuario.setNombre("Prueba");
		usuario.setApellido("Prueba");
		usuario.setCorreoElectronico("noreply.summit.tracking@gmail.com");
		usuario.setMovil("51-66-8-985");
		usuario.setRoles(roles);
		usuario.setTipoUsuario("Interno");
		usuario.setPassword("+Y(W7yaY");
		usuario.setFechaHoraInicioSesion(Calendar.getInstance().getTime());
		
		Compania compania = new Compania();
		compania.setRazonSocial("Semillas S.A de C.V");
		
		companiaRepository.save(compania);
		
		usuario.setCompania(compania);
		
		Usuario usuarioRecuperado = usuarioService.registrarNuevaCuentaUsuario(usuario);
		
		assertNotNull(usuarioRecuperado.getId());
		
	}
	
	@Test
	public void debeEditarUsuarioTest() {
		log.debug("*********Entrando al metodo UsuarioServiceImplTest.debeEditar usuarioTest**************");
		Usuario usuario1 = new Usuario();
		usuario1.setCorreoElectronico("ingrid.casales@tecno.mx");
		usuario1.setMovil("556311239");
		usuario1.setNombre("Ingrid");
		usuario1.setApellido("Casales");
		usuario1.setTipoUsuario("Interno");
		usuario1.setPassword(passwordEncoder.encode("abc123"));
		List<String> roles = new ArrayList<String>();
		roles.add("ROLE_USUARIO");
		usuarioRepository.save(usuario1);
		usuario1.setMovil("5567844935");
		roles.add("ROLE_ADMIN");
		Usuario usuario2 = usuarioService.editarUsuario(usuario1, roles);
		assertTrue(usuario2.getRoles().size() > 1);

	}
	@Test
	public void debeListarTodosUsuarios(){
		List<Usuario> usuarios = usuarioService.obtenerTodosUsuarios();
		assertTrue(usuarios.size() > 0);
	}
	@Test
	public void debeInhabilitarUsuario(){
		log.debug("*********Entrando al metodo UsuarioServiceImplTest.debeInhabilitarUsuario Test**************");
		Usuario usuario2 = new Usuario();
		usuario2.setCorreoElectronico("victorpruebas@tecno.mx");
		usuario2.setMovil("556311239");
		usuario2.setNombre("Ingrid");
		usuario2.setApellido("Casales");
		usuario2.setTipoUsuario("Interno");
		usuario2.setHabilitado(true);
		usuario2.setPassword(passwordEncoder.encode("abc123"));
		List<String> roles = new ArrayList<String>();
		roles.add("ROLE_USUARIO");
		usuarioRepository.save(usuario2);
		usuario2.setMovil("5567844935");
		roles.add("ROLE_ADMIN");
		assertTrue(usuario2.isHabilitado() == true );
		Usuario usuarioActual = usuarioService.obtenerUsuarioPorCorreo(usuario2.getCorreoElectronico());
		usuario2.setHabilitado(false);
		usuarioActual.setHabilitado(usuario2.isHabilitado());
		usuarioService.habilitarUsuario(usuarioActual);
		assertTrue(usuarioActual.isHabilitado() == false);

	}

	
	@AfterEach
	public void afterEach() {
		
		Usuario usuarioRecuperado = usuarioRepository.findByCorreoElectronico("noreply.summit.tracking@gmail.com");
		if(usuarioRecuperado != null)
			usuarioRepository.delete(usuarioRecuperado);
		
		usuarioRecuperado = usuarioRepository.findByCorreoElectronico("ingrid.casales@tecno.mx");
		if(usuarioRecuperado != null)
			usuarioRepository.delete(usuarioRecuperado);
		Usuario usuarioRecuperado2 = usuarioRepository.findByCorreoElectronico("victorpruebas@tecno.mx");
		if(usuarioRecuperado2 != null)
			usuarioRepository.delete(usuarioRecuperado2);
	}
	
	
}
