package mx.com.tecno.tracking.service.impl;

import static org.junit.jupiter.api.Assertions.fail;

import javax.mail.MessagingException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.service.MailService;

@SpringBootTest(classes= {TrackingApplication.class})
@Slf4j
public class MailServiceImplTest {
	
	@Autowired
	private MailService mailService;
	
	@Test
	public void enviarMensaje() {
		log.debug("Entrando a enviarMensaje");
		
		try {
			mailService.send("noreply.summit.tracking@gmail.com", "Prueba", "Estes es el cuerpo del mensaje");
		} catch(MessagingException me) {
			
			fail("Hubo un error al mandar el correo electronico");
			
		}
		
		
		 
	} 

}
