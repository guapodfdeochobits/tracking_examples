package mx.com.tecno.tracking.service.impl;

import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.domain.Compania;
import mx.com.tecno.tracking.service.CatalogoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes =  {TrackingApplication.class})
public class CompaniaServiceImplTest {
    @Autowired
    CatalogoService catalogoService;
    @Test
    public void debeListarCompanias(){
        List<Compania> companiaList = catalogoService.obtenerTodasCompanias();
        assertTrue(companiaList.size() > 0);
    }
}
