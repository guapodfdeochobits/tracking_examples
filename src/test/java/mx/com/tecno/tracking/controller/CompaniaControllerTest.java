package mx.com.tecno.tracking.controller;

import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.domain.VerificacionTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.ResultMatcher.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import javax.servlet.ServletContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.web.servlet.function.RequestPredicates.contentType;

@SpringBootTest(classes = {TrackingApplication.class})
public class CompaniaControllerTest {
    private MockMvc mockMvc;
    @Autowired
    private CatalogosController controller;
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private VerificacionTokenRepository verificacionTokenRepository;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser(username = "oscar.ortiz@summitagromexico")
    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @WithMockUser(username = "oscar.ortiz@summitagromexico")
    @Test
    public void debeObtenerJson() throws Exception {
        this.mockMvc.perform(get("/catalogos/companias").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$", hasItems(notNullValue())))
        .andExpect(jsonPath("$[0].id",is(1)));
    }
   /* @WithMockUser(username = "oscar.ortiz@summitagromexico")
    @Test
    public void debeCargarPagina() throws Exception{
        this.mockMvc.perform(get("/catalogos")).andExpect(status().isOk())
                .andExpect(xpath("//button[@name='search']").exists());
    }*/
}
