package mx.com.tecno.tracking.controller;
 
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import javax.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.UsuarioRepository;
import mx.com.tecno.tracking.domain.VerificacionToken;
import mx.com.tecno.tracking.domain.VerificacionTokenRepository;

@SpringBootTest(classes= {TrackingApplication.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class UsuarioControllerTest {
	
	
	private  MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext context;
	
	@Autowired
	private VerificacionTokenRepository verificacionTokenRepository;
	

	private static  UsuarioRepository usuarioRepository;
	


    @Autowired
    public UsuarioControllerTest(UsuarioRepository usuarioRepository) {
    	UsuarioControllerTest.usuarioRepository = usuarioRepository;
    }
    
    
    @BeforeEach
    public void setup() {
    	mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

	
    @WithMockUser(username="oscar.ortiz@summitagromexico")
	@Test
	@Order(1)
	public void debeRegistrarUsuario() throws Exception {

		log.debug("Entrando a debeRegistrarUsuario");
		
		
		mockMvc.perform(post("/usuario/agregar")
				.param("nombre", "Prueba")
				.param("apellido", "Prueba")
				.param("correoElectronico", "noreply.summit.tracking@gmail.com")
				.param("movil", "51-66-8-985")
				.param("password", "summit01")
				.param("tipoUsuario", "Interno")
				.param("rol", "ROLE_USUARIO"))
		.andExpect(redirectedUrl("/agregar-usuario?exitoso=true"));
		
	}
	
    @WithMockUser
	@Test
	@Order(2)
	public void debeConfirmarUsuario() throws Exception {
		log.debug("Entrando a debeConfirmarUsuario");
				
		VerificacionToken verificacionToken =  verificacionTokenRepository
				.findByEmail("noreply.summit.tracking@gmail.com");
		
		mockMvc.perform(get("/usuario/confirmar")
				.param("token", verificacionToken.getToken()))
				.andExpect(redirectedUrl("/index?lang=en&message=Cuenta verificada!"));
		
		
	}
	
    @WithAnonymousUser
	@Test
	@Order(3)
	public void debeIniciarSesion() {
		
		log.debug("Entrando a debeIniciarSesion");
		
		try {
			mockMvc.perform(post("/j_spring_security_check")
					.param("j_username", "noreply.summit.tracking@gmail.com")
					.param("j_password", "summit01"));
		} catch (Exception e) {
			fail("Hubo un error al iniciar sesion");
		}
		
	}
    
	
    @WithMockUser(username="oscar.ortiz@summitagromexico")
	@Test
	@Order(4)
	public void debeBorrarUsuario() throws Exception {
		
		log.debug("Entrando a debeBorrarUsuario");
		
		Usuario usuario = usuarioRepository.findByCorreoElectronico("noreply.summit.tracking@gmail.com");
  
		this.mockMvc.perform(delete("/usuario/borrar/{usuarioId}", String.valueOf(usuario.getId().intValue())))
				.andExpect(redirectedUrl("/index"));
		
		
	}
    
    @WithMockUser(username="oscar.ortiz@summitagromexico")
   	@Test
   	@Order(5)
   	public void debeCargarPaginaAgregarUsuario() throws Exception {
   		
   		log.debug("Entrando a debeCargarPaginaAgregarUsuario");
   		
   		final ServletContext servletContext = context.getServletContext();
   		
   		this.mockMvc.perform(get("/usuario/cargar-agregar-usuario"))
   				.andExpect(redirectedUrl("/agregar-usuario"));
   				
   		assertNotNull(servletContext.getAttribute("companias"));
   		
   	}
	
	
}
