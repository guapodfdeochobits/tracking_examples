package mx.com.tecno.tracking.controller;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lombok.extern.slf4j.Slf4j;
import mx.com.tecno.tracking.TrackingApplication;
import mx.com.tecno.tracking.domain.Usuario;
import mx.com.tecno.tracking.domain.UsuarioRepository;

@SpringBootTest(classes = { TrackingApplication.class })
@AutoConfigureMockMvc
@Slf4j
public class AuditoriaControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext context;
	
	
	@Autowired
	private  UsuarioRepository usuarioRepository;

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}
	
	@WithMockUser(username="sergio.mena@tecno.com.mx")
	@Test
	@Order(1)
	public void debeAgregarUsuarioTest() throws Exception {
		
		log.debug("Entrando al método AuditoriaControllerTest.debeAgregarUsuarioTest");
		
		mockMvc.perform(post("/usuario/agregar")
				.param("nombre", "Prueba")
				.param("apellido", "Prueba")
				.param("correoElectronico", "noreply.summit.tracking@gmail.com")
				.param("movil", "51-66-8-985")
				.param("password", "summit01")
				.param("tipoUsuario", "Interno")
				.param("rol", "ROLE_USUARIO"))
				.andExpect(redirectedUrl("/agregar-usuario?exitoso=true"));
		
	}
	
	@WithMockUser(username="sergio.mena@tecno.com.mx")
	@Test
	@Order(2)
	public void debeActualizarUsuarioTest() throws Exception {
		
		log.debug("Entrando al método AuditoriaControllerTest.debeActualizarUsuarioTest");
		mockMvc.perform(put("/usuario/actualizar").param("correoElectronico", "noreply.summit.tracking@gmail.com")
				.param("movil", "555555555"))
				.andExpect(redirectedUrl("/index"));
		
		Usuario usuario = usuarioRepository.findByCorreoElectronico("noreply.summit.tracking@gmail.com");
		
		if (usuario != null) {
			mockMvc.perform(delete("/usuario/borrar/{usuarioId}", String.valueOf(usuario.getId().intValue())))
			.andExpect(redirectedUrl("/index"));
	
		}
		
	}

	@WithMockUser(username="sergio.mena@tecno.com.mx")
	@Test
	@Order(3)
	public void debeObtenerAuditoriaTodas() throws Exception {

		log.debug(">Entrando a debeObtenerAuditoriaTodas()<");
		
		mockMvc.perform(get("/auditoria/obtenerToda?page=1&size=10"))
				.andExpect(status().isOk());
				

	}

}
